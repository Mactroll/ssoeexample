# Simple SSO Extension Example

This is an overly simplified example of how an SSOE can be done.

The app comes in two pieces.

- The Host application which really is nothing more then a vestigial vessel for the app extension inside.

- The actual SSOE itself. This is where the actual "meat" of the process lives.

In order to get this SSOE to work you'll need to do a number of things.

1. Add your target domain to the Associated Domains entitlement of the application. You'll see that example.nomad.menu has already been set. Replace this with a domain of your choosing.
2. Host an Apple App Site Association file on that domain. This needs to be served from `.well-known/apple-app-site-association` and sent as json. More info and a quick way of testing this can be found here: https://branch.io/resources/aasa-validator/
3. Push an SSOE configuration profile from a user-approved MDM source. You cannot even test an SSOE without having a profile provided by a user-approved MDM. This means your test system will have to be enrolled in a MDM service that you control.

Assuming all of that is completed correctly... you probably need to reboot before things work. <sigh>

After that, going to a URL that has the `apple-app-site-association` file and is configured in your profile, will pop up the sheet with a browser in it. Once done doing authentication or other operation, the user can then use the "Inject" button to return all cookies back to the calling application and reload the original URL. For most modern authentication methods this will pass the authentication back to the caller.

Note: This is most likely not the way you'd want to do this! However, it should illustrate how the Redirect extension works and what you can do with it.

To further show what can be done with an SSOE, the "Apple" button will return a 302 redirect to the calling application sending the app to "https://www.apple.com".
